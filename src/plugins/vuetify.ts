import Vue from 'vue';
import Vuetify from 'vuetify';
import ru from 'vuetify/src/locale/ru';

Vue.use(Vuetify);

export default new Vuetify({
    lang: {
      locales: { ru },
      current: 'ru',
    },
    theme: {
        themes: {
            dark: {
                primary: '#42a5f6',
                secondary: '#050b1f',
                accent: '#204165',
            },
            light: {
                primary: '#42a5f6',
                secondary: '#050b1f',
                accent: '#f14419',
            }
        }
    }
});
